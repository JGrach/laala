// This API is a singleton, need to be required once before usage

const { readdirSync } = require("fs")
const { readFile } = require("fs/promises")
const path = require('path')

function initPages(viewsDir) {
    return readdirSync(viewsDir).reduce((acc, filePath) => {
        const pageName = filePath.slice(0, filePath.length - '.html'.length).toUpperCase()
        return {...acc, [pageName]: filePath}
    }, {})
}

function Client() {
    this.VIEWS_DIR = path.resolve(__dirname, 'views')
    this.STATIC_DIR = path.resolve(__dirname, 'static')
    this.getPage = (pagePath) => readFile(path.resolve(`${this.VIEWS_DIR}/${pagePath}`), 'utf-8')
    this.pages = initPages(this.VIEWS_DIR)
}


module.exports = new Client()