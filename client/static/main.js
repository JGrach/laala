function handleScore() {
    let counter = 0
    const score = document.getElementById("score")
    return function incrementScore() {
        counter ++
        score.innerHTML=counter
    }
}

(function() {
    const incrementScore = handleScore()

    const iframe = document.getElementById("laala-screen-game")
    iframe.onload = function() {
        incrementScore()
    };
})()
