const express = require('express')
const router = require('./routes')
const app = express()

app.use('/laala', express.static('client/static'))
app.use(router)
app.listen(process.env.PORT || 7000)

