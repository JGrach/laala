const { getPage, pages } = require('../client');
const { fetchWikipedia, WIKIPEDIA_URL } = require("../api/wikipedia");
const { changeWikipediaPage } = require('./modifyWikipedia')

function indexController(req, res, next) {
    getPage(pages.INDEX)
        .then(res.send.bind(res))
        .catch(next)
}

function wikipediaController(req, res, next) {
    const resource = req.url.slice('/page'.length)
    fetchWikipedia(resource)
        .then(changeWikipediaPage)
        .then(res.send.bind(res))
        .catch(next)
}


function metadataController(req, res) {
    res.redirect(`${WIKIPEDIA_URL}/${req.url}`)
}

module.exports = {
    indexController, 
    wikipediaController,
    metadataController
}