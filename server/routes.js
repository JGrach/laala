const { Router } = require('express')
const { indexController, wikipediaController, metadataController } = require('./controllers')

const router = Router()

router.get('/', indexController)
router.get('/page/*', wikipediaController)
router.get('/*', metadataController)

module.exports = router