const { parseHtml } = require("../api/document")
const { addToHead, setStyle, setScript } = require("../api/dom")

function appendBehavior(dom) {
    const cssOverrideWikistyle = setStyle(dom,'/laala/override_wikistyle.css')
    addToHead(dom, cssOverrideWikistyle)
}

function tagUselesses(dom) {
    const headers = [...dom.getElementsByClassName("mw-header")]
    const navigation = [dom.getElementById("mw-navigation")]
    const lang = [dom.getElementById("p-lang-btn")]
    const indicators = [...dom.getElementsByClassName("mw-indicators")]
    const editors = [...dom.getElementsByClassName("mw-editsection")]

    return [...headers, ...navigation, ...lang, ...indicators, ...editors]
        .filter(e => e!= null)
        .forEach(el => el.classList.add('disabled'));
}

function manageLinks(dom) {
    const links = [...dom.getElementsByTagName('a')]
    links.forEach(link => {
        const href = link.getAttribute('href')
        link.setAttribute('href', `http://localhost:7000/page/${href}`)
    })
}

function changeWikipediaPage(page) {
    const html = parseHtml(page)
    const dom = html.window.document

    appendBehavior(dom)
    tagUselesses(dom)
    manageLinks(dom)

    return html.serialize()
}

module.exports = { changeWikipediaPage }