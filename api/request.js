const { get } = require('https')

function fetch(url) {
    return new Promise((res, rej) => { // TODO: what if no event fired ?
        const clientRequest = get(url)

        clientRequest.on("response", res)
        clientRequest.on("close", () => rej(new Error("Request closed without response")))
    })
}

function readResponseData(response) { // TODO: what if no event fired ? ex: statusCode 301 => no data
    return new Promise((res, rej) => {
        let data = ''
        response.on("data", c => data += c)
        response.on("error", rej)
        response.on("close", () => res(data))
    })
}

module.exports = {
    fetch,
    readResponseData
}
