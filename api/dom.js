function addToHead(dom, ...elements) {
    const { head } = dom
    elements.forEach(element => head.insertBefore(element, head.firstChild)); 
}

function setStyle(dom, path) {
    const link = dom.createElement('link')
    link.setAttribute('href', path)
    link.setAttribute('rel', 'stylesheet')
    return link
}

function setScript(dom, path) {
    const script = dom.createElement('script')
    script.setAttribute('src', path)
    script.setAttribute('type', 'text/javascript')
    return script
}

module.exports = {
    addToHead,
    setStyle,
    setScript
}