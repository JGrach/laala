const { JSDOM } = require("jsdom")

function parseHtml(content) {
    return new JSDOM(content)
} 

return module.exports = {
    parseHtml
}