const { fetch, readResponseData } = require('./request')

const WIKIPEDIA_URL = "https://fr.wikipedia.org"

function fetchWikipedia(url) {
    return fetch(`${WIKIPEDIA_URL}/${url}`)
        .then(readResponseData)
}

module.exports = {
    WIKIPEDIA_URL,
    fetchWikipedia
}
